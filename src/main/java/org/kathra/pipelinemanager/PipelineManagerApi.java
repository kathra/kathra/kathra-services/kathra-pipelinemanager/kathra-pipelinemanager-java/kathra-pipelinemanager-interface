/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.pipelinemanager;

import org.kathra.utils.security.KeycloakUtils;
import org.kathra.core.model.Build;
import org.kathra.pipelinemanager.model.Credential;
import org.kathra.core.model.Membership;
import org.kathra.core.model.Pipeline;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.kathra.KathraAuthRequestHandlerImpl;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.rest.RestBindingMode;
import static org.apache.camel.model.rest.RestParamType.*;

@ContextName("PipelineManager")
public class PipelineManagerApi extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        KeycloakUtils.init();
        // configure we want to use servlet as the component for the rest DSL
        // and we enable json binding mode
        restConfiguration().component(org.kathra.iface.KathraAuthRequestHandler.HTTP_SERVER)
        // use json binding mode so Camel automatic binds json <--> pojo
        .bindingMode(RestBindingMode.off)
        // and output using pretty print
        .dataFormatProperty("prettyPrint", "true")
        .dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES")
        // setup context path on localhost and port number that netty will use
        .contextPath("/api/v1")
        .port("{{env:HTTP_PORT:8080}}")
        .componentProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .endpointProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .consumerProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))

        // add swagger api-doc out of the box
        .apiContextPath("/swagger.json")
        .apiProperty("api.title", "Kathra Pipeline Manager")
        .apiProperty("api.version", "1.2.0")
        .apiProperty("api.description", "PipelineManager")
        // and enable CORS
        .apiProperty("cors", "true")
        .enableCORS(true).corsAllowCredentials(true)
        .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type," +
                "Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

        rest()

        .post("/credentials").type(Credential.class).outType(Credential.class)
            .description("Creates a credential for a specified folder")
                .param()
                    .required(true)
                    .name("credential")
                    .type(body)
                    .description("Credential to create")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,Credential.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=addCredential(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .post("/memberships").type(Membership.class).outType(Membership.class).produces("application/json")
            .description("Creates a membership for specified folder")
                .param()
                    .required(true)
                    .name("membership")
                    .type(body)
                    .description("Credential to create")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,Membership.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=addMembership(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .post("/builds").type(Build.class).outType(Build.class).produces("application/json")
            .description("Creates a Build")
                .param()
                    .required(true)
                    .name("build")
                    .type(body)
                    .description("Build to run, must contain path & branch")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,Build.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=createBuild(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .post("/folders").outType(Boolean.class)
            .description("Create a new folder")
                .param()
                    .required(true)
                    .name("path")
                    .type(body)
                    .description("Folder path")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=createFolder(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .post("/pipelines").type(Pipeline.class).outType(Pipeline.class).produces("application/json")
            .description("Create CI/CD pipeline for an existing source")
                .param()
                    .required(true)
                    .name("pipeline")
                    .type(body)
                    .description("Pipeline to create, must contain path & source repository")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,Pipeline.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=createPipeline(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .delete("/pipelines/{path}").outType(String.class).produces("application/json")
            .description("Delete pipeline")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("path")
                    .type(path)
                    .description("Pipeline path")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=deletePipeline(${header.path})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/pipelines/{path}/builds/{buildNumber}").outType(Build.class).produces("application/json")
            .description("Gets a Build")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("path")
                    .type(path)
                    .description("Build identifier")
                .endParam()
                .param()
                    .required(true)
                    .dataType("string")
                    .name("buildNumber")
                    .type(path)
                    .description("Build identifier")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=getBuild(${header.path},${header.buildNumber})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/pipelines/{path}/builds").outType(Build[].class).produces("application/json")
            .description("Gets a list of Builds")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("path")
                    .type(path)
                    .description("Pipeline path")
                .endParam()
                .param()
                    .required(false)
                    .dataType("string")
                    .name("branch")
                    .type(query)
                    .description("Builded branch")
                .endParam()
                .param()
                    .required(false)
                    .dataType("integer")
                    .name("maxResult")
                    .type(query)
                    .description("Max results")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=getBuilds(${header.path},${header.branch},${header.maxResult})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/folders/{path}/memberships").outType(Membership[].class)
            .description("Get memberships for specified folder")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("path")
                    .type(path)
                    .description("Pipeline path")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:PipelineManagerController?method=getMemberships(${header.path})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest();
    }
}