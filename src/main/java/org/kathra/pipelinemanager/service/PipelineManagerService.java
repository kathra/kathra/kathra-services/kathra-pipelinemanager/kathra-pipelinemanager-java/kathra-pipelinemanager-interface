/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.pipelinemanager.service;

import org.kathra.core.model.Build;
import org.kathra.pipelinemanager.model.Credential;
import org.kathra.core.model.Membership;
import org.kathra.core.model.Pipeline;
import java.util.List;

public interface PipelineManagerService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Creates a credential for a specified folder
    * 
    * @param credential Credential to create (required)
    * @return Credential
    */
    Credential addCredential(Credential credential) throws Exception;

    /**
    * Creates a membership for specified folder
    * 
    * @param membership Credential to create (required)
    * @return Membership
    */
    Membership addMembership(Membership membership) throws Exception;

    /**
    * Creates a Build
    * 
    * @param build Build to run, must contain path & branch (required)
    * @return Build
    */
    Build createBuild(Build build) throws Exception;

    /**
    * Create a new folder
    * 
    * @param path Folder path (required)
    * @return Boolean
    */
    Boolean createFolder(String path) throws Exception;

    /**
    * Create CI/CD pipeline for an existing source
    * 
    * @param pipeline Pipeline to create, must contain path & source repository (required)
    * @return Pipeline
    */
    Pipeline createPipeline(Pipeline pipeline) throws Exception;

    /**
    * Delete pipeline
    * 
    * @param path Pipeline path (required)
    * @return String
    */
    String deletePipeline(String path) throws Exception;

    /**
    * Gets a Build
    * 
    * @param path Build identifier (required)
    * @param buildNumber Build identifier (required)
    * @return Build
    */
    Build getBuild(String path, String buildNumber) throws Exception;

    /**
    * Gets a list of Builds
    * 
    * @param path Pipeline path (required)
    * @param branch Builded branch (optional)
    * @param maxResult Max results (optional, default to 5)
    * @return List<Build>
    */
    List<Build> getBuilds(String path, String branch, Integer maxResult) throws Exception;

    /**
    * Get memberships for specified folder
    * 
    * @param path Pipeline path (required)
    * @return List<Membership>
    */
    List<Membership> getMemberships(String path) throws Exception;

}
